# Calculator

Poor man's calculator program in C.

## Running

Dependency: GCC

```bash
git clone https://framagit.org/TheEvilSkeleton/calculator
gcc ./src/main.c -o calculator
./calculator
```
