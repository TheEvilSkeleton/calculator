#include <stdio.h>

int main(void)
{
  // Set variables
  int operation = 0;
  float num1 = 0;
  float num2 = 0;
  float answer = 0;
  // Start 'while' statement
  while(operation < 1 || operation > 4)
    {
      // Print initial question
      printf("Addition, subtraction, multiplication or division?\n0. Quit\n1. Addition\n2. Subtraction\n3. Multiplication\n4. Division\n\nEnter number:\n[0,1,2,3,4]: ");
      // Scan the value of 'operation'
      scanf("%d", &operation);
      // If 'operation' is 0, exit the program
      if(operation == 0)
        {
          printf("Goodbye.\n");
          return 0;
        }
      // Addition initial
      else
        {
          printf("Select the first number.\n>> ");
          scanf("%f", &num1);
          // Ask for the second number
          printf("Select the second number.\n>> ");
          scanf("%f", &num2);
          // Addition
          if (operation == 1)
            {
              answer = num1 + num2;
            }
          // Subtraction
          else if (operation == 2)
            {
              answer = num1 - num2;
            }
          // Multiplication
          else if (operation == 3)
            {
              answer = num1 * num2;
            }
          // Division
          else
            {
              answer = num1 / num2;
            }
        }
    }
  // Print answer
  printf("%f\n", answer);
  // Exit
  return 0;
}
